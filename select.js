(function (_, ng) {
    'use strict';
    ng
    .module('formRenderer.controls')
    .service('frFieldTypeService-form-renderer-select-control', [

        /**
         * frFieldTypeSelectService
         * @return {Object} - field type configuration object
         */
        function frFieldTypeSelectService() {
            return {
                link: function (scope, element, attrs, ctrl) {

                    var frFormCtrl = ctrl[0];
                    var dataStore;

                    function initialize() {
                        if (scope.field.to.dataStore) {
                            dataStore = frFormCtrl.getDataStore(scope.field.to.dataStore);
                            if (dataStore.length > 0) {
                                scope.field.to.valueOptions = dataStore;
                            }
                        }
                    }

                    /**
                     * @function
                     * @name  getValueFromOptions -----------------------
                     * @description - get a value from the value options by key
                     * @param  {String} key - value option key
                     * @return {String} - option value
                     */
                    scope.getValueFromOptions = function getValueFromOptions(key) {
                        var option = _.find(scope.field.to.valueOptions, function (option) {
                            return option.key === key;
                        });
                        return option && option.value;
                    };


                    initialize();
                }
            };
        }
    ]);
})(window._, window.angular);
